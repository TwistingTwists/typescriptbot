import { Bot, InputFile } from 'grammy'


// 1. Create a bot
export const bot = new Bot(process.env.BOT_TOKEN as string, {
    client: {
        // 2. Set the local Bot API URL
        apiRoot: 'http://0.0.0.0:8082',
    },
})
// Handle the /start command.
bot.command("start", (ctx) => ctx.reply("Welcome! Up and running."));
// Handle other messages.
// bot.on("message", (ctx) => ctx.reply("Got another message!"));

bot.on("message:voice", async (ctx) => {
    const voice = ctx.msg.voice;

    const duration = voice.duration; // in seconds
    await ctx.reply(`Your voice message is ${duration} seconds long.`);

    const fileId = voice.file_id;
    await ctx.reply("The file identifier of your voice message is: " + fileId);

    const file = await ctx.getFile(); // valid for at least 1 hour
    const path = file.file_path; // file path on Bot API server

    await ctx.reply("Download your own file again: " + path);
});
// bot.on('message:text', ctx => ctx.reply(ctx.message.text))

bot.on(':file', async (ctx) => {
    const file = await ctx.getFile();
    await ctx.replyWithDocument(new InputFile(file.file_path || "./.gitignore"))
})

// 3. Start the bot
bot.start()