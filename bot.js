"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.bot = void 0;
const grammy_1 = require("grammy");
// 1. Create a bot
exports.bot = new grammy_1.Bot(process.env.BOT_TOKEN, {
    client: {
        // 2. Set the local Bot API URL
        apiRoot: 'http://localhost:8082',
    },
});
exports.bot.on('message:text', ctx => ctx.reply(ctx.message.text));
// 3. Start the bot
exports.bot.start();
